!window.vueComponents && (window.vueComponents = {});
window.vueComponents.app = {
  children: {
    "todo-list":vueComponents.todoList,
    "nav-footer":vueComponents.navFooter,
    "info-footer":vueComponents.infoFooter,
  },
  template:`
    <div>
      <section id="todoapp">
				<header id="header">
					<h1>todos</h1>
					<form id="todo-form" @submit="addTodo()">
						<input id="new-todo" placeholder="What needs to be done?" v-model="newTodo" :disabled="saving" autofocus>
					</form>
				</header>
				<section id="main" v-show="todos.length">
					<input id="toggle-all" type="checkbox" v-model="allChecked" @click="markAll(allChecked)">
					<label for="toggle-all">Mark all as complete</label>
					<todo-list :todos="todos" :status-filter="statusFilter" :edited-todo="editedTodo"/>
				</section>
        <nav-footer :status="status" :todos="todos"/>
			</section>
			<info-footer/>
    </div>
  `,
  data() {
    return {
      store: LocalStorage,
      todos: [],
      newTodo:{},
      saving:false,
      saveEvent:null,
      originalTodo:{},
      editedTodo:{},
      reverted: false,
      statusFilter: {}
    };
  },
  methods: {
    addtodo() {
			let newTodo = {
				title: this.newTodo.trim(),
				completed: false
			};
			if (!newTodo.title) {
				return;
			}
      this.saving = true;
			store.insert(newTodo)
				.then(() => this.newTodo = '')
				.finally(() => this.saving = false);
		},
    removeTodo(todo) {
      this.store.delete(todo);
    },
    editTodo(todo) {
      this.editedTodo = todo;
      // Clone the original todo to restore it on demand.
      this.originalTodo = Object.assign({}, todo);
    },
    revertEdits(todo) {
      let currentTodo = this.todos[todos.indexOf(todo)];
      currentTodo.title = this.originalTodo.title;
      currentTodo.completed = this.originalTodo.completed;
      this.editedTodo = null;
      this.originalTodo = null;
      this.reverted = true;
    },
    saveEdits(todo, $event) {
      // Blur events are automatically triggered after the form submit event.
      // This does some unfortunate logic handling to prevent saving twice.
      if ($event === 'blur' && this.saveEvent === 'submit') {
        this.saveEvent = null;
        return;
      }
      this.saveEvent = event;
      if (this.reverted) {
        // Todo edits were reverted-- don't save.
        this.reverted = null;
        return;
      }

      todo.title = todo.title.trim();
      if (todo.title === this.originalTodo.title) {
        this.editedTodo = null;
        return;
      }
      this.store[todo.title ? 'put' : 'delete'](todo)
        .then(() => {})
        .catch(() => todo.title = this.originalTodo.title)
        .finally(() => this.editedTodo = null);
    },
    toggleCompleted(todo, completed) {
      typeof completed !== 'undefined' && (todo.completed = completed);
      this.store.put(todo, this.todos.indexOf(todo))
        .catch(() => todo.completed = !todo.completed);
    },
    getStatusFilter(event) {
      const status = event.newURL.split('/#/')[1] || '';
      this.statusFilter = {};
      status === 'active' && (this.statusFilter = {completed: false});
      status === 'completed' && (this.statusFilter = {completed: true});
      window.location.hash = `/#/${status}`;
    },
  },
  created() {
    TodoStorage.getStorageObject().then( object => {
      this.store = object;
      this.store.get()
        .then(todos => this.todos = todos)
        .then(todos => this.$emit('todosLoaded', todos, store));
    });
    //temporary until vue-router
    this.getStatusFilter({newURL: '/' + window.location.hash});
    window.addEventListener('hashchange', this.getStatusFilter)
  }
}
