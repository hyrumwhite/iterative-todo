!window.vueComponents && (window.vueComponents = {});
window.vueComponents.todoList = {
  template: `
    <ul id="todo-list">
      <li v-for="(todo, index) of filteredTodos" :class="{completed: todo.completed, editing: todo == editedTodo}">
        <div class="view">
          <input class="toggle" type="checkbox" v-model="todo.completed" @change="$emit('toggleCompleted', todo, todo.completed)">
          <label @click="$emit('editTodo', todo)">{{todo.title}}</label>
          <button class="destroy" @click="$emit('removeTodo', todo)"></button>
        </div>
        <form @submit="$emit('saveEdits', todo)">
          <input ref="editor" class="edit" v-model="todo.title"  @keyup.esc="$emit('revertEdits', todo)"
            @blur="$emit('saveEdits', todo, 'blur')">
        </form>
      </li>
    </ul>
  `,
  props: {
    todos: {
      type: Array,
      default: () => []
    },
    statusFilter:{
      type: Object,
      default: () => ({})
    },
    editedTodo: {
      type: Object,
      default: () => ({})
    }
  },
  computed: {
    filteredTodos() {
      return this.todos.filter(item => {
        return typeof this.statusFilter.completed === 'undefined' || item.completed === this.statusFilter.completed;
      });
    }
  },
  methods: {
    focusEditor() {
      this.$refs.editor[0].focus();
    }
  }
}
