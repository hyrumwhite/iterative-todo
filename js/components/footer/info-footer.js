!window.vueComponents && (window.vueComponents = {});
window.vueComponents.infoFooter = {
  el:'#info-footer',
  template:`
    <footer id="info-footer">
      <p>Click to edit a todo</p>
      <p>Credits:
        <a href="http://twitter.com/cburgdorf">Christoph Burgdorf</a>,
        <a href="http://ericbidelman.com">Eric Bidelman</a>,
        <a href="http://jacobmumm.com">Jacob Mumm</a> and
        <a href="http://blog.igorminar.com">Igor Minar</a>
      </p>
      <p>Part of <a href="http://todomvc.com">TodoMVC</a></p>
    </footer>
  `
};
