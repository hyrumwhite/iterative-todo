!window.vueComponents && (window.vueComponents = {});
window.vueComponents.navFooter = {
  el: "#nav-footer",
  template: `
    <footer id="footer" v-show="todos.length">
      <span id="todo-count"><strong>{{remainingCount}}</strong>
        {{itemsLeft}}
      </span>
      <ul id="filters">
        <li>
          <a :class="{selected: status == ''} " href="#/">All</a>
        </li>
        <li>
          <a :class="{selected: status == 'active'}" href="#/active">Active</a>
        </li>
        <li>
          <a :class="{selected: status == 'completed'}" href="#/completed">Completed</a>
        </li>
      </ul>
      <button id="clear-completed" @click="$emit('clearCompletedTodos')" v-show="completedCount">Clear completed</button>
    </footer>
  `,
  props: {
    status: {
      type: String,
      default:''
    },
    todos: {
      type: Array,
      default: () => []
    }
  },
  computed: {
    itemsLeft() {
      return this.todos.filter(({completed}) => !completed).length === 1 ? "item left" : 'items left';
    },
    remainingCount() {
      return this.todos.filter(({completed}) => !completed).length;
    },
    completedCount() {
      return this.todos.filter(({completed}) => completed).length;
    }
  }
}
