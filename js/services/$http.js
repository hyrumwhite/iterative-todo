class $http {
  static get(url) {
    return window.fetch(url).then(response => ({data: response.json()}));
  }
}
