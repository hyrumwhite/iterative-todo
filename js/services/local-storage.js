class LocalStorage {
  constructor() {
    this.STORAGE_ID = 'todos-angularjs';
    this.todos = [];
  }
  _getFromLocalStorage() {
    return JSON.parse(localStorage.getItem(STORAGE_ID) || '[]');
  }
  _saveToLocalStorage(todos) {
    localStorage.setItem(STORAGE_ID, JSON.stringify(todos));
  }
  clearCompleted() {
    return new Promise((resolve, reject) => {
      var incompleteTodos = this.todos.filter(({completed}) => !completed);
      this.todos = incompleteTodos;
      this._saveToLocalStorage(this.todos);
      resolve(this.todos);
    });
  }
  delete(todo) {
    return new Promise((resolve, reject) => {
      this.todos.splice(this.todos.indexOf(todo), 1);
      this._saveToLocalStorage(this.todos);
      resolve(this.todos);
    });
  }
  get() {
    return new Promise(resolve => resolve(this._getFromLocalStorage));
  }
  insert(todo) {
    return new Promise(resolve => {
      this.todos.push(todo);
      this._saveToLocalStorage(this.todos);
      resolve(this.todos);
    })
  }
  put(todo, index) {
    return new Promise(resolve => {
      this.todos[index] = todo;
      this._saveToLocalStorage(this.todos);
      resolve(this.todos);
    })
  }
}
