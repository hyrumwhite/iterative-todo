class TodoStorage {
	static getStorageObject() {
		return $http.get('/api')
			.then(response => new Api())
			.catch(error => new LocalStorage());
	}
}
